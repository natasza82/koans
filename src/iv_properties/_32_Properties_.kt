package iv_properties

class PropertyExample() {
    var counter = 0
    private var sth = 0
    var propertyWithCounter: Int
        set(value) {
            sth = value
            counter++
        }
        get() = sth
}
/*
    """
        Task 32.
        Add a custom setter to PropertyExample.propertyWithCounter so that
        the 'counter' property is incremented every time 'propertyWithCounter' is assigned to.
    """,
    documentation = doc32(),
    references = { PropertyExample() }*/

