package iv_properties

import kotlin.reflect.KProperty

class LazyPropertyUsingDelegates(val initializer: () -> Int) {
    val lazyValue: Int by Delegate()
}

/*
        """
        Task 34.
        Read about delegated properties and make the property lazy by using delegates.
    """,
        documentation = doc34()
*/

class Delegate {
    var isUsed = false
    var value = 0
    operator fun getValue(lazyPropertyUsingDelegates: LazyPropertyUsingDelegates, property: KProperty<*>): Int {
        if (!isUsed) {
            value = lazyPropertyUsingDelegates.initializer()
            isUsed = true
        }
        return value
    }
}
