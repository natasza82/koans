package iv_properties

class LazyProperty(val initializer: () -> Int) {
    var isUsed = false
    var value = 0
    val lazy: Int
        get() {
            if (!isUsed) {
                value = initializer()
                isUsed = true
            }
            return value
        }
}

/*
        """
        Task 33.
        Add a custom getter to make the 'lazy' val really lazy.
        It should be initialized by the invocation of 'initializer()'
        at the moment of the first access.
        You can add as many additional properties as you need.
        Do not use delegated properties!
    """,
        references = { LazyProperty({ 42 }).lazy }
*/