package iii_conventions

class Invokable {

    var counter = 0

    operator fun invoke(): Invokable {
        counter++
        return this
    }

    fun getNumberOfInvocations(): Int {
        return counter
    }
}

/*
    """
        Task 31.
        Change class Invokable to count the number of invocations (round brackets).
        Uncomment the commented code - it should return 4.
    """,
    references = { invokable: Invokable -> })*/

fun task31(invokable: Invokable): Int {
    return invokable()()()().getNumberOfInvocations()
}
