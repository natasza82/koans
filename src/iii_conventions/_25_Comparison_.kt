package iii_conventions

/*    """
        Task 25.
        Uncomment the commented line and make it compile. 
        Add all changes to the file MyDate.kt.
        Make class MyDate implement Comparable.
    """,
    documentation = doc25(),
    references = { date: MyDate, comparable: Comparable<MyDate> -> }*/

fun task25(date1: MyDate, date2: MyDate): Boolean {
    return date1 < date2
}

