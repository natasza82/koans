package iii_conventions

import java.util.*

data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int) : Comparable<MyDate> {

    override fun compareTo(other: MyDate): Int {
        if (0 != year.compareTo(other.year)) return year.compareTo(other.year)
        if (0 != month.compareTo(other.month)) return month.compareTo(other.month)
        return dayOfMonth.compareTo(other.dayOfMonth)
    }

    operator fun plus(interval: TimeInterval) = this.addTimeIntervals(interval, 1)
}


operator fun MyDate.rangeTo(other: MyDate) = DateRange(this, other)

operator fun MyDate.plus(tiv: RepeatedTimeInterval) = this.addTimeIntervals(tiv.ti, tiv.n)

enum class TimeInterval {
    DAY,
    WEEK,
    YEAR
}

operator fun TimeInterval.times(n: Int): RepeatedTimeInterval = RepeatedTimeInterval(this, n)

class RepeatedTimeInterval(val ti: TimeInterval, val n: Int)

class DateRange(override val start: MyDate, override val endInclusive: MyDate) : ClosedRange<MyDate>, Iterable<MyDate> {
    override fun iterator(): Iterator<MyDate> = object : Iterator<MyDate> {
        private var nextValue: MyDate = start

        override fun hasNext(): Boolean {
            if (nextValue > endInclusive)
                return false
            return true
        }

        override fun next(): MyDate {
            if (!hasNext()) {
                throw NoSuchElementException()
            }

            val currentValue = nextValue
            nextValue = nextValue.nextDay()
            return currentValue
        }
    }

}





