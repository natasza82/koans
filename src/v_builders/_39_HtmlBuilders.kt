package v_builders

import v_builders.data.getProducts
import v_builders.htmlLibrary.*

fun getTitleColor() = "#b9c9fe"
fun getCellColor(row: Int, column: Int) = if ((row + column) % 2 == 0) "#dce4ff" else "#eff2ff"

/*
        """
        Task 39.
        1) Fill the table with the proper values from products.
        2) Color the table like a chess board (using getTitleColor() and getCellColor() functions above).
        Pass a color as an argument to functions 'tr', 'td'.
        You can run the 'Html Demo' configuration in IntelliJ IDEA to see the rendered table.
    """,
        documentation = doc39()
*/

fun renderProductTable(): String {
    return html {
        table {
            tr(getTitleColor()) {
                td {
                    text("Product")
                }
                td {
                    text("Price")
                }
                td {
                    text("Popularity")
                }
            }
            var ax = 0
            val products = getProducts()
            products.map { tr { td(getCellColor(ax, 0)) { text(it.description) };
                td(getCellColor(ax, 1)) { text(it.price) }; td(getCellColor(ax, 2))
                { text(it.popularity) } };ax++ }
        }
    }.toString()
}
